# Projet Martin Geometry Dash STM32

## Résumé
Projet de jeu Geometry Dash (en version simplifiée) sur une carte STM32F746G-Discovery utilisant la librairie LittleVGL et l'OS FreeRTOS.

Photo du jeu (Geometry Dash) pour lequel je me suis inspiré pour réaliser ce projet :
![Photo jeu Geometry Dash](https://gitlab.com/mini_projet_442/projet_martin_geometry_dash/-/blob/main/Images/geometry_dash.jpg)

## Installation
Ce projet fonctionne sur une carte STM32746G-Discovery dans le langage C. Il faut installer STM32CubeIDE pour pouvoir le faire fonctionner sur cette carte.

## Utilisation de LVGL dans ce project

Pour plus d'informations et d'exemples sur comment utiliser LVGL (notamment ses widgets) : 
[Documentation LVGL](https://docs.lvgl.io/master/index.html)

Pour ce projet, on utilise des canvas pour créer le joueur et les obstacles ([Pour plus d'informations, cliquez ici](https://docs.lvgl.io/master/widgets/canvas.html))

On place dans ces canvas soit un carré (pour le joueur et le bloc), soit un polygone pour la pic. 

## Partie FreeRTOS

### Liste des taches

#### Tache 1 : gestion_saut

Lorsqu'un message est transmis via la messagerie MessagerieSaut, cette tâche va récupérer le message et le stocker dans la variable saut. Puis, elle va lancer la fonction realiser_saut(saut) afin que le joueur réalise un saut.

#### Tache 2 : LVGL_Tick

Tâche nécessaire au bon fonctionnement de LVGL. Permet de rafraichir l'affichage tous les 50 ms en fournissant à LVGL une échelle de temps.

#### Tache 3 : gestion_deplacement

Tous les 50 ms, cette tâche se lance et va modifier les variables xpic1 et xbloc1, mettant ainsi les obstacles en mouvement qui se déplacent vers la gauche

#### Tache 4 : tache_gestion

Gère les différents types d'appui sur l'écran :
- Appui sur le bouton recommencer : envoi d'un message via la messagerie MessagerieGameOver permettant de lancer la fonction recommencer_niveau() via la tâche gestionGameOver et donc de recommencer le niveau
- Appui sur le bouton pause : on suspend les tâches gestion_deplacement et gestion_saut ce qui permet de mettre en pause le jeu
La pause se finit lors du prochain appui sur l'écran
- Appui sur l'écran quelconque : envoi d'un message via MessagerieSaut permettant de lancer la fonction realiser_saut() via la tâche gestion_saut et donc de faire sauter le joueur

#### Tache 5 : gestionGameOver

Lorsqu'un message est transmis via la messagerie MessagerieSaut, cette tâche va récupérer le message puis va lancer la fonction recommencer_niveau() afin de recommencer le niveau.

#### Tache 6 : gestion_collision

Gère les collisions entre le joueur et les obstacles. Lorsqu'il y a collision, on envoie un message via MessagerieGameOver afin de recommencer le niveau. Puis, on actualise le compteur de morts (on ajoute 1 à celui-ci)


### Messageries

#### MessagerieSaut

On transmet un message via cette messagerie lors du relachement après un appui sur l'écran.

#### MessagerieGameOver

On transmet un message via cette messagerie lorsqu'il y a collision entre le joueur et un des obstacles (bloc ou pic) ou lors de l'appui sur le bouton Recommencer.

### Fonctions annexes

#### recommencer_niveau()

Réinitialise le niveau c'est-à-dire les positions du joueur, des blocs et des pics

#### realiser_saut(uint8_t saut)

Permet au joueur de réaliser un saut. Le saut se fait sur place et est la combinaison de 2 choses : 
- le joueur fait un tour sur lui-même
- au même moment, le joueur monte puis redescend
Avec les obstacles se déplacant vers la gauche, cela permet de simuler simplement un saut réalisé par le joueur.

## Utilisation de LVGL sur STM32

Voici un lien vers le git d'un projet réalisé par LeRatonLaveurSolitaire utilisant également LVGL sur une STM32F746G :
[Projet en question](https://github.com/LeRatonLaveurSolitaire/projet_embarque) 

Ce git comprend un tutoriel pour mettre en oeuvre LVGL sur une carte STM32F746G-Discovery tout en utilisant l'OS FreeRTOS.

Si vous rencontrez des difficultés, voici la documentation de LVGL au sujet de l'utilisation de LVGL sur une STM32 :
[Documentation LVGL portage sur STM32](https://docs.lvgl.io/master/get-started/platforms/stm32.html)

## Améliorations possibles 
Le jeu aurait pu aller plus loin mais faute de temps, il reste très simple. Le but étant juste d'utiliser la librairie LiitleVGL sur une STM32F746G-Discovery
Voici des améliorations possibles pour mon projet :
- Ajouter plus d'obstacles
- Diversifier le type d'obstacles rencontrés
- Mettre en place un système basique de gravité permettant de monter sur des blocs et d'en descendre
- Mettre en place un système de niveaux avec une difficulté croissante

